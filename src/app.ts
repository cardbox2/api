import express from 'express';
import mongoose from 'mongoose';
import { Router } from './routes/core/router';
import { Kernel } from './middlewares/kernel';
import { environment } from './environment/dev';

// Mongoose Initialization 
mongoose.connect(environment.mongoURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    autoIndex: true, 
    keepAlive: true,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    family: 4, 
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Express Initialization
const app = express();

// Core Middlewares
Kernel.registerCoreMiddlewares(app);

// Routing
Router.registerRoutes(app);

app.listen(process.env.PORT || 8080)
.on('error', (err:any) => { if (err) { return console.error(err); } })
.on('listening', () => { return console.log('Server is listening on ' + environment.port); });