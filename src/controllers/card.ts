import { Request, Response } from 'express';
import { Schema } from 'mongoose';
import { Card } from '../schemas/card';

export const CardController = {
  // - POST - /cards # Get all cards
  get: (req: Request, res: Response) => {
    Card.find()
      .where('_id')
      .in(req.body.ids)
      .exec(function (err, card) {
        if (err) {
          res.status(500).json({ code: 500, message: err });
        } else {
          res.json(card);
        }
      });
  },

  // - GET - /card/:id # Get specific card
  getCard: (req: Request, res: Response) => {
    Card.findById(req.params.id).exec(function (err, card) {
      if (err) {
        res.status(500).json({ code: 500, message: err });
      } else {
        res.json(card);
      }
    });
  },

  // - CREATE - /card # Create a new card
  create: (req: Request, res: Response) => {
    console.log(req.body);
    let card = new Card(req.body);
    card.save((err) => {
      if (err) {
        res.status(500).json({ code: 500, message: err });
      } else {
        res.json(card);
      }
    });
  },

  // - UPDATE - /card/:id # Update a card
  update: (req: Request, res: Response) => {
    Card.findOneAndUpdate({ _id: req.params.id }, req.body).exec(function (
      err,
      card
    ) {
      if (err) {
        res.status(500).json({ code: 500, message: err });
      } else {
        res.json(card);
      }
    });
  },

  // - DELETE - /card/:id # Update a card
  delete: (req: Request, res: Response) => {
    Card.findOneAndDelete({ _id: req.params.id }, req.body).exec(function (
      err,
      card
    ) {
      if (err) {
        res.status(500).json({ code: 500, message: err });
      } else {
        res.json(card);
      }
    });
  },
};
