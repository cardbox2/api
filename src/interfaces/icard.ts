export interface ICard {
  number: String;
  expiration: String;
  cvv: String;
}
