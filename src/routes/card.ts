import { Route } from './core/route';

// Controllers
import { CardController } from '../controllers/card';

// Middlewares
import { HTTP_METHODS } from './core/methods';

export const CARD_ROUTES: Array<any> = [
  new Route({
    url: '/cards',
    middlewares: [],
    method: HTTP_METHODS.POST,
    controller: CardController.get,
  }),
  new Route({
    url: '/card/:id',
    middlewares: [],
    method: HTTP_METHODS.GET,
    controller: CardController.getCard,
  }),
  new Route({
    url: '/card',
    middlewares: [],
    method: HTTP_METHODS.POST,
    controller: CardController.create,
  }),

  new Route({
    url: '/card/:id',
    middlewares: [],
    method: HTTP_METHODS.UPDATE,
    controller: CardController.update,
  }),

  new Route({
    url: '/card/:id',
    middlewares: [],
    method: HTTP_METHODS.DELETE,
    controller: CardController.delete,
  }),
];
