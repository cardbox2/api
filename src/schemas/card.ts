import { Document, Schema, Model, model } from 'mongoose';
import { ICard } from '../interfaces/icard';

export interface ICardModel extends ICard, Document {}

export var CardSchema: Schema = new Schema(
  {
    number: {
      type: String,
      required: true,
    },
    expiration: {
      type: String,
      required: true,
    },
    cvv: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

export const Card: Model<ICardModel> = model<ICardModel>('Card', CardSchema);
